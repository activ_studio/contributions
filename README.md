# Contributions

Most of Activ Studio contributions are made with other account names or other repositories/website. We’ll "try" to maintain here a list of them.

Follow : https://gitlab.com/gemy_cedric/

## Projects :

### Scribus

    - contribution to official doc
    - patch : display master page name in organize page window
    - patch : organize files in subdir when collecting for output
    - mentoring : picture browser

### Inkscape
    
    - official doc in french (english up to 2007)
    - export group as single files (python ported to cpp by bulya)
    - black theme polishing on Fill and stroke window
    - webdesign/goto export
    - CMYK export via scribus

### Gimp
    
    - official doc in french (upto 2005)
    - godot exporter

### Blender
    
    - bug reports

### Godot Engine
    - Gimp/inkscape to godot exporter

### Krita
    - Krita 3-fold fr

### FlossManuals fr
    - several books :
        - Initiation à Inkscape (Elisa)
        - Initiation à Prestashop (Elisa)
        - Initiation à Wordpress (Elisa)
        - Initiation à Gimp (Cedric)
        - Initiation à Scribus (Cedric)
        - Initiation à Python (Cedric)
        - Initiation à PHP (Cedric)
        - Initiation à JS (Cedric)
        - Initiation à HTML (Cedric)
        - Initiation à CSS (Cedric)
        - Initiation à Django (Cedric)
    - several book contribution
        - Scribus
        - Fontes Libres
        - Créer un epub
        - Blender pour l’impression 3D
        - Blender pour le jeu vidéo
        - Blender pour le montage vidéo
        - Reprap
        - Fablab
        - Créer des jeux avec Godot
        - Créer des points and click avec Escoria
        
### AFGRAL
    - Grafik Labor

### Debian
    - public website for debian 9.0 (Elisa)

### UpStage
    - UX and designs (Elisa)

### Librazik
    - OS design, webdesign (Elisa)
